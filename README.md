# IDS 721 Mini Project 10
This project demonstrates the process of dockerize transformer, deploy container to AWS Lambda and implement query endpoint

## Build Process:
Follow these steps to set up and run your project:

### Step 1: Project Setup

1. **Create a New Rust Project:**
   Create a new Project using the following command: 
   ```
   cargo lambda new <Your-project-name>
   ```
   Add dependencies to cargo.toml: 
   ```
   [dependencies]
    lambda_http = "0.11.1"
    tokio = { version = "1", features = ["macros", "rt-multi-thread"] }
    llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
    openssl = { version = "0.10.35", features = ["vendored"] }
    serde = {version = "1.0", features = ["derive"] }
    serde_json = "1.0"
    rand = "0.8.5"
   ```

2. **Add Functionalities:**
    Add codes in main.rs to add in functionalities. Also, include the model file in the project, which is `pythia-410m-q4_0-ggjt.bin` in my case.

3. **Test your project locally:**
    Run the following command to test your code locally: 
    `cargo lambda watch`

#### Containarize the Project: 
1. Create a new private repository in AWS ECR. 
![Alt text](/screenshots/ECR.png)
2. Retrieve an authentication token and authenticate your Docker client to your registry using the following command: `aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 533267197894.dkr.ecr.us-east-1.amazonaws.com`
3. Build your Docker image using the following command: `docker build -t dl402 .`
4. After the build completes, tag your image so you can push the image to this repository: `docker tag dl402:latest 533267197894.dkr.ecr.us-east-1.amazonaws.com/dl402:latest`
5. Run the following command to push this image to your newly created AWS repository: `docker push 533267197894.dkr.ecr.us-east-1.amazonaws.com/dl402:latest` Then we can see that the image is pushed to ECR repo:
![Alt text](/screenshots/image.png)

#### AWS Lambda Function: 
1. Create a new Lambda function using a container image. Remember to choose the image you pushed to ECR.
2. Create a new function URL enabling CORS. Adjust the memory size and timeout length accordingly so that the model can reply.
3. Create a function URL for the lambda function for testing.
![Alt text](/screenshots/lambda.png)

Use `curl https://td4mhke5k4el7cmyjhjaziicnu0kfhku.lambda-url.us-east-1.on.aws/\?text=Python%20is%20the%20best%20language%20because` to check the results:

![Alt text](/screenshots/result.png)

